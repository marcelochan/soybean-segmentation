# Soybean segmentation

##Image segmentation

##Author: Marcelo Chan Fu Wei

###This file is part of the evaluation from the class Image Processing -SCC5830-2021 given by Professor Moacir Antonelli Ponti

###Summary: Soybean plants at R8 maturity stage are close to the harvest period. Therefore, their leaves fall and the plant stem and pods become brownish. In field conditions, most of the areas present the soil (background) with brown colors and adding the fallen leaves which are also brown, make the segmentation process to become challeging since all the objects within the scheme are brown-related. It will be used images acquired by the author with soybean plants at the R8 maturity stage taken from a proximal RGB sensor (cellphone) with the objective to segment the soybean plant among the background.

###Image segmentation is a technique that has been part of several processing steps for agricultural purposes. For example, the ability to separate the background and the target object from a given image can improve the application of image classification and/or counting. Therefore, as an initial step, it is important to have a proper image segmentation algorithm before an image classification and/or counting algorithm.

###Soybean plant at the R8 maturity stage is close to the harvesting period, thus getting plant arquicteture or structure may help farmers/breeders during their yield evaluation. However, obtaining the plant structure at R8 stage is highly difficult since the plant and the background (soil, fallen leaves, dead plants and others) usually have similar color and texture. Hence, it becomes an opportunity to evaluate the possibility of applying image processing techniques to segment soybean plant and background at the R8 maturity stage.

###The goal of this project is to segment the background and the soybean plant at the R8 maturity stage from RGB images for agricultural purposes.

###Input images will be composed of images taken by the author with a sensor from a cellphone in-field situation at the R8 phenological stage

###The process is based on the following steps:

1. Loading image

2. Converting image from RGB to Luminance/L*a*b*

3*. Verify image histogram and Luminance/L* image result

4*. Set the threshold (upper/lower) value depending on the image histogram

5. Test the application of a Median Filter to reduce noise in the image

6*. Test the application of morphological operators (erosion followed by dilation) with different structures (square, diamond and disk)

7. Plot images to visualize the results from the segmentation process

#3*: The use of RGB to Luminance yielded better results than L* from L*a*b*

#4*: It was tested the use of mean threshold, Otsu threshold and "manually" selected threshold. Setting the threshold image by image yielded better results than using the other methods that calculated the values during the process.

#6*: The use of erosion followed by dilation did reduce the noise in the image, however, they also removed important parts from the soybean plant. Therefore, the use of morphological operators must be carefully chosen by the user.

###The author tried to make some image segmentation process and yielded some results, however, it is highlighted that the complexity of the type of image used to be segmented made this task more challeging which allow other people to improve these steps or suggest alternative methods to cope with this particular problem.


###Dataset used can be found at "Dataset.zip"

###There is also the codes in the files as "Final Project - Soybean plant segmentation in field conditions at R8 maturity stage-Versao Final.py" and "Final Project - Soybean plant segmentation in field conditions at R8 maturity stage-Versao Final.ipynb"

###A ".pdf" report is also available and it can be seen at "Final Project - Soybean plant segmentation in field conditions at R8 maturity stage.pdf"

July 13 2021
