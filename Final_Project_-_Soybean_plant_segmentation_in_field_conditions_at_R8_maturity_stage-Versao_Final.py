#!/usr/bin/env python
# coding: utf-8

# Marcelo Chan Fu Wei
# 
# N.USP: 10630558
# 
# Final Project - Soybean plant segmentation in field conditions at R8 maturity stage
# 
# SCC5830 - Image Processing
# 
# Prof. Moacir Ponti

# Summary
# 
# Soybean plants at R8 maturity stage are close to the harvest period. Therefore, their leaves fall and the plant stem and pods become brownish. In field conditions, most of the areas present the soil (background) with brown colors and adding the fallen leaves which are also brown, make the segmentation process to become challeging since all the objects within the scheme are brown-related. It will be used images acquired by the author with soybean plants at the R8 maturity stage taken from a proximal RGB sensor (cellphone) with the objective to segment the soybean plant among the background.

# Main objective: segment soybean plant at R8 stage from the background
# Input images: images taken from a RGB sensor (cellphone) at field conditions with soybean plants at maturity stage R8 (check Dataset file)
# Description of steps and methods:
# 1. Loading image
# 2. Converting image from RGB to Luminance/L*a*b*
# 3. Verify image histogram and Luminance/L* image result
# 4. Set the threshold (upper/lower) value depending on the image histogram
# 5. Test the application of a Median Filter to reduce noise in the image
# 6. Test the application of morphological operators (erosion followed by dilation) with different structures (square, diamond and disk)
# 7. Plot images to visualize the results from the segmentation process

# In[2]:


#Libraries required
import numpy as np
import imageio
import matplotlib.pyplot as plt
from skimage import morphology
from skimage.morphology import (square, diamond, disk, star)
import skimage.filters


# In[4]:


#Set of images
soybean0 = imageio.imread('IMG_20190325_113727854.jpg')
soybean1 = imageio.imread('IMG_20190325_113733080.jpg')
soybean2 = imageio.imread('IMG_20190325_114501857.jpg')
soybean3 = imageio.imread('IMG_20190329_114131057.jpg')
soybean4 = imageio.imread('IMG_20190329_114423017.jpg')
soybean5 = imageio.imread('IMG_20190224_151800632.jpg')


# In[6]:


#Showing the input images
plt.figure(figsize=(16,12))

plt.subplot(321); plt.title("Soybean0", size = 15); plt.imshow(soybean0); 
plt.subplot(322); plt.title("Soybean1", size = 15); plt.imshow(soybean1); 
plt.subplot(323); plt.title("Soybean2", size = 15); plt.imshow(soybean2);
plt.subplot(324); plt.title("Soybean3", size = 15); plt.imshow(soybean3);
plt.subplot(325); plt.title("Soybean4", size = 15); plt.imshow(soybean4); 
plt.subplot(326); plt.title("Soybean5", size = 15); plt.imshow(soybean5);


# In[7]:


#Convert RGB to Linear rgb
def linear (value):
    new_value = np.zeros_like(value)
    for x in range(value.shape[1]):
        for y in range(value.shape[0]):
            if value[y,x] < 0.04045:
                new_value[y,x] = value[y,x]/12.92
            else:
                new_value[y,x] = ((value[y,x] + 0.055)/1.055)**2.4

    return new_value


# In[8]:


#function f related to t
def f(t):
    if (t > (6/29)**3):
        return np.power(t, 1/3.0);
    else:
        return ((1/3)*(29/6)**2)*t + (4/29);


# In[9]:


#RGB to Lab
#First it is necessary to convert RGB to XYZ, them XYZ to Lab
#Convert RGB from 0-255 to 0 - 1
def rgbtolab(img):
    R = img[:, :, 0]/255
    G = img[:, :, 1]/255
    B = img[:, :, 2]/255


    #Linear rgb

    r = linear(R)
    g = linear(G)
    b = linear(B)

    X = (0.4124*r + 0.3576*g + 0.1805*b)*100
    Y = (0.2126*r + 0.7152*g + 0.0722*b)*100
    Z = (0.0193*r + 0.1192*g + 0.9505*b)*100

    #Converting XYZ to Lab

    L= np.zeros_like(img)
    a= np.zeros_like(img)
    b= np.zeros_like(img)
    for x in range(img.shape[1]):
        for y in range(img.shape[0]):           
            #Calculating L    
            L[y,x] = 116* f(Y[y,x]) - 16

            #Calculating a
            a[y,x] = 500*(f(X[y,x]) - f(Y[y,x]))

            #Calculating b
            b[y,x] = 200*(f(Y[y,x]) - f(Z[y,x]))
    
    #Normalizing L to 0-100
    L_norm = L.astype(np.uint32)*100/np.max(L)
    
    return L_norm.astype(np.uint8), a, b




# In[10]:


#RGB to luminance
def Luminance(img):
    #Create a copy of the original image
    img = np.array(img, copy=True).astype(int)
    # Converting RGB to Luminance
    new_img = np.zeros((img.shape[0], img.shape[1]))
    #RGB to Luminance = R*0.299 + G*0.587 + B*0.114
    new_img = ((img[:,:,0]*0.299 + img[:,:,1]*0.587 + img[:,:,2]*0.114)).astype(np.uint8)
   
    return new_img


# In[11]:


#Function to threshold the image based on a given upper and lower limit value
def threshold (img, upper_limit, lower_limit):
    
    #create a matrix with zeros with the same size as the image input
    img_zero_t = np.zeros_like(img).astype(np.uint32)

    #change the values below and above the limits to zero and keep the remaining ones
    for x in range(img.shape[1]):
        for y in range(img.shape[0]):
            if img[y,x] > upper_limit: #Attempt to remove brighter pixels - in this case, it can be the square object
                img_zero_t[y,x] = 0
            elif img[y,x] < lower_limit: #Attempt to remove darker pixels - in this case, it can be the shadows
                img_zero_t[y,x] = 0
            else:
                img_zero_t[y,x] = img[y,x]

    
    return img_zero_t


# In[12]:


#Median filter to remove noise with a given n kernel size
def medianFilter(img, n):
    #Creating a new image filled with zeros
    img_median = np.zeros(img.shape)
    for y in range(n//2, img.shape[0]-n//2):
        for x in range(n//2, img.shape[1]-n//2):
            img_median[y,x] = np.median(img[y-n//2:y+n//2+1, x-n//2:x+n//2+1])

    return img_median


# In[13]:


#Function to define the size of the output image =
def size(img):

#Input image has either size 4032 x 3024 or 3024 x 4032

    if img.shape[1]>img.shape[0]:
        a = img.shape[1]//320 #It was used 320 because we want the output image to have size 16
        b = img.shape[0]//180 #It was used 180 because we want the output image to have size 12
        size = (a,b)

    elif img.shape[0]>img.shape[1]:
        a = img.shape[0]//320
        b = img.shape[1]//180
        size = (b,a)
    return size


# In[14]:


#Test image 0

#Convert RGB to Luminance

soybean0_lumi = Luminance(soybean0)

#Convert RGB to Lab

L_img0, a_img0, b_img0 = rgbtolab(soybean0)

#Calculate the size of the output image
size_img0 = size(soybean0)


# In[40]:


#Histogram plot
plt.figure(figsize=(size_img0))
hist_img0_lumi,_ = np.histogram(soybean0_lumi, bins = 256, range = (0,256))
hist_img0_l,_ = np.histogram(L_img0, bins = 100, range = (0,100))
hist_original,_ = np.histogram(soybean0, bins = 256, range = (0,256))
plt.subplot(321); plt.title("Original", size = 25); plt.imshow(soybean0); 
plt.subplot(322); plt.title("Histogram", size = 25); plt.bar(np.arange(0,256), hist_original);
plt.subplot(323); plt.title("Luminance", size = 25); plt.imshow(soybean0_lumi); 
plt.subplot(324); plt.title("Histogram", size = 25); plt.bar(np.arange(0,256), hist_img0_lumi); 
plt.subplot(325); plt.title("L*", size = 25); plt.imshow(L_img0); 
plt.subplot(326); plt.title("Histogram", size = 25); plt.bar(np.arange(0,100), hist_img0_l); 


# Looking at the converted image RGB to Luminance, we can notice two peaks (one brighter - close to 250 and one darker - close to 0), thus, we might be able to remove them from the image.
# Looking at the converted image RGB to L*a*b* only at the L*, we can see that if we try to apply a threshold to segment background and plant, it will be difficult since all the scheme seem to be the same. Therefore, based on this results, we will proceed using only the Luminance method.

# In[15]:


#From the histogram, it was tested and decided to use as upper limit value 120 and lower limit value 50. 
#However, we will show the results using Otsu and mean threshold (only for the lower value) in this example
mean0 = np.mean(soybean0_lumi)
img0_zero = threshold(soybean0_lumi, 120, 50)
img0_zero_mean = threshold(soybean0_lumi, 120, mean0)


# In[16]:


#Otsu threshold
t = skimage.filters.threshold_otsu(soybean0_lumi)

#Creating a mask with values higher than the Otsu Threshold
mask = soybean0_lumi > t

#selecting the desired values within the image
sel = np.zeros_like(soybean0_lumi)
sel[mask] = soybean0_lumi[mask]

otsu_img0 = sel - soybean0_lumi


# In[37]:


#Printing the Otsu threshold (t) and mean threshold value (mean0)
print(t)
print(mean0)


# Otsu threshold value used was 108 and mean threshold was 96.78.

# In[17]:


#Image filter aplying median filter with n = 10
segmented_median0_n10 = medianFilter(img0_zero, 10)
#Image filter aplying median filter with n = 5
segmented_median0_n5 = medianFilter(img0_zero, 5)


# In[18]:


#Plotting the images
plt.figure(figsize=(size_img0))


plt.subplot(321); plt.title("Original", size = 15); plt.imshow(soybean0); 
plt.subplot(322); plt.title("Segmented manual threshold", size = 15); plt.imshow(img0_zero); 
plt.subplot(323); plt.title("Segmented mean threshold", size = 15); plt.imshow(img0_zero_mean);
plt.subplot(324); plt.title("Segmented Otsu threshold", size = 15); plt.imshow(otsu_img0);
plt.subplot(325); plt.title("Segmented + Median filter n =10", size = 15); plt.imshow(segmented_median0_n10); 
plt.subplot(326); plt.title("Segmented + Median filter n =5", size = 15); plt.imshow(segmented_median0_n5);


# From the images above, comparing the segmented images with "manual threshold", "mean threshold" and "Otsu threshold", it can be noticed that manual threshold yields better results. Otsu threshold was not able to set the shadows as background. These results are particular for this dataset. Therefore, it was decided to proceed the morphological operators using only the results from the segmented images that were manually set (one by one) and filtered applying Median Filter with n = 10.

# In[19]:


#It was tested the square, diamond and disk morphology operators
img_erod_diam = morphology.binary_erosion(segmented_median0_n10, morphology.diamond(6)).astype(np.uint8)
img_di_diam = morphology.binary_dilation(img_erod_diam, morphology.diamond(6)).astype(np.uint8)

img_erod_sq = morphology.binary_erosion(segmented_median0_n10, morphology.square(6)).astype(np.uint8)
img_di_sq = morphology.binary_dilation(img_erod_sq, morphology.square(6)).astype(np.uint8)

img_erod_disk = morphology.binary_erosion(segmented_median0_n10, morphology.disk(6)).astype(np.uint8)
img_di_disk = morphology.binary_dilation(img_erod_disk, morphology.disk(6)).astype(np.uint8)

plt.figure(figsize=(size_img0))
plt.subplot(321); plt.title("Eroded image - diamond(6)"); plt.imshow(img_erod_diam)
plt.subplot(322); plt.title("Dilated image - diamond(6)"); plt.imshow(img_di_diam)

plt.subplot(323); plt.title("Eroded image - disk(6)"); plt.imshow(img_erod_disk)
plt.subplot(324); plt.title("Dilated image - disk(6)"); plt.imshow(img_di_disk)

plt.subplot(325); plt.title("Eroded image - square(6)"); plt.imshow(img_erod_sq)
plt.subplot(326); plt.title("Dilated image - square(6)"); plt.imshow(img_di_sq)


# Note from the above images that it was possible to remove some noisy points. However, it also eliminated desired points if we compare to the image "Segmented image + Median filter". Therefore, for this specific case, the processing until median filter yielded better results compared to the use of additional morphological operators.
# 
# From the current results and aware that the images 0,1 and 2 are more similar in terms of histogram than images 3, 4 and 5, it is not possible to apply the same thresholding values for images 3, 4 and 5. 

# In[20]:


#Function for images from 0 to 2
def segmentation(img):
    soybean0_lumi = Luminance(img)

    #Calculate the size of the output image
    size_img0 = size(img)

    #Histogram plot
    plt.figure(figsize=(size_img0))
    hist_img0,_ = np.histogram(soybean0_lumi, bins = 256, range = (0,256))
    plt.subplot(321); plt.title("Luminance", size = 25); plt.imshow(soybean0_lumi); 
    plt.subplot(322); plt.title("Histogram", size = 25); plt.bar(np.arange(0,256), hist_img0); 
    img0_zero = threshold(soybean0_lumi, 120, 50) #Upper and lower value set as 120 and 50, respectively
    segmented_median0_n10 = medianFilter(img0_zero, 10)
    segmented_median0_n5 = medianFilter(img0_zero, 5)
   

    plt.figure(figsize=(size_img0))
    plt.subplot(323); plt.title("Original", size = 15); plt.imshow(img); 
    plt.subplot(324); plt.title("Segmented", size = 15); plt.imshow(img0_zero); 
    plt.subplot(325); plt.title("Segmented + Median filter n =10", size = 15); plt.imshow(segmented_median0_n10); 
    plt.subplot(326); plt.title("Segmented + Median filter n =5", size = 15); plt.imshow(segmented_median0_n5);
    
    
    #img_erod = morphology.binary_erosion(segmented_median0_n5, morphology.diamond(6)).astype(np.uint8)
    #img_di = morphology.binary_dilation(img_erod, morphology.diamond(6)).astype(np.uint8)
    #plt.figure(figsize=(size_img0))
    #plt.subplot(327); plt.title("Eroded image - diamond(6)"); plt.imshow(img_erod)
    #plt.subplot(328); plt.title("Dilated image - diamond(6)"); plt.imshow(img_di)


# In[21]:


segmentation(soybean1)


# In[415]:


segmentation(soybean2)


# In[22]:


#Test case 3

soybean3_lumi = Luminance(soybean3)

#Calculate the size of the output image
size_img3 = size(soybean3)

#Histogram plot
plt.figure(figsize=(size_img3))
hist_img3,_ = np.histogram(soybean3_lumi, bins = 256, range = (0,256))
plt.subplot(321); plt.title("Original", size = 25); plt.imshow(soybean3); 
plt.subplot(322); plt.title("Luminance", size = 25); plt.imshow(soybean3_lumi); 
plt.subplot(323); plt.title("Histogram", size = 25); plt.bar(np.arange(0,256), hist_img3); 


# Note how the histogram of case3 is different from case 0, 1 and 2. Therefore, we cannot use the same threshold values before.This different may occured becasue of the difference in brightness.

# In[23]:


#Note that the square contains pixels larger than 200, thus, we will remove it.
plt.imshow(soybean3_lumi>200)


# In[25]:


#create a matrix with zeros with the same size as the image input
img3_zero_t = np.zeros_like(soybean3_lumi).astype(np.uint32)
mean3 = np.mean(soybean3_lumi)
#change the values below and above the limits to zero and keep the remaining ones
for x in range(soybean3_lumi.shape[1]):
    for y in range(soybean3_lumi.shape[0]):
        if soybean3_lumi[y,x] > 200: #In this case, we want to remove the square
            img3_zero_t[y,x] = 0
        elif soybean3_lumi[y,x] < mean3: #Setting the lower value as the mean
            img3_zero_t[y,x] = 0
        else:
            img3_zero_t[y,x] = soybean3_lumi[y,x]


# In[27]:


#Median filter application
segmented_median3_n10 = medianFilter(img3_zero_t, 10)
segmented_median3_n5 = medianFilter(img3_zero_t, 5)


# In[28]:


#Plotting the images
plt.figure(figsize=(size_img3))
plt.subplot(323); plt.title("Original", size = 15); plt.imshow(soybean3); 
plt.subplot(324); plt.title("Segmented", size = 15); plt.imshow(img3_zero_t); 
plt.subplot(325); plt.title("Segmented + Median filter n =10", size = 15); plt.imshow(segmented_median3_n10); 
plt.subplot(326); plt.title("Segmented + Median filter n =5", size = 15); plt.imshow(segmented_median3_n5);


# In[430]:


#Test case 4

soybean4_lumi = Luminance(soybean4)

#Calculate the size of the output image
size_img4 = size(soybean4)

#histogram plot
plt.figure(figsize=(size_img4))
hist_img4,_ = np.histogram(soybean4_lumi, bins = 256, range = (0,256))
plt.subplot(321); plt.title("Original", size = 25); plt.imshow(soybean4); 
plt.subplot(322); plt.title("Luminance", size = 25); plt.imshow(soybean4_lumi); 
plt.subplot(323); plt.title("Histogram", size = 25); plt.bar(np.arange(0,256), hist_img4); 


# In[452]:


#create a matrix with zeros with the same size as the image input
img4_zero_t = np.zeros_like(soybean4_lumi).astype(np.uint32)
mean4 = np.mean(soybean4_lumi)
#change the values below and above the limits to zero and keep the remaining ones
for x in range(soybean4_lumi.shape[1]):
    for y in range(soybean4_lumi.shape[0]):
        if soybean4_lumi[y,x] > 200: #In this case, we want to remove the square
            img4_zero_t[y,x] = 0
        elif soybean4_lumi[y,x] < mean4: #Set the values below the mean to zero
            img4_zero_t[y,x] = 0
        else:
            img4_zero_t[y,x] = soybean4_lumi[y,x]


# In[444]:


#Median filter application
segmented_median4_n10 = medianFilter(img4_zero_t, 10)
segmented_median4_n5 = medianFilter(img4_zero_t, 5)


# In[453]:


#Plotting the images
plt.figure(figsize=(size_img3))
plt.subplot(323); plt.title("Original", size = 15); plt.imshow(soybean4); 
plt.subplot(324); plt.title("Segmented", size = 15); plt.imshow(img4_zero_t); 
plt.subplot(325); plt.title("Segmented + Median filter n =10", size = 15); plt.imshow(segmented_median4_n10); 
plt.subplot(326); plt.title("Segmented + Median filter n =5", size = 15); plt.imshow(segmented_median4_n5);


# Note from the above images, that segmentation process did not yield good results. Part of the soil was not able to set as background. From this image, you can see that soil color are close to the plant color, making the segmentation process harder. Aware of these in-field situations, we can notice that thresholding values (upper and/or lower) must be carefully chosen to make the best as possible segmentation. Also, we highlight that threshold values might be better used when chosen according to each image.

# Case 5 is an image that the soybean plant were inserted into a place far from the cultivated spot. This was made on purpose because it was necessary to check if a segmentation process could yield good results in image with more distinguised objects.

# In[31]:


#Test case 5

soybean5_lumi = Luminance(soybean5)

#Calculate the size of the output image
size_img5 = size(soybean5)

#Histogram plot
plt.figure(figsize=(size_img5))
hist_img5,_ = np.histogram(soybean0_lumi, bins = 256, range = (0,256))
plt.subplot(321); plt.title("Luminance", size = 25); plt.imshow(soybean5_lumi); 
plt.subplot(322); plt.title("Histogram", size = 25); plt.bar(np.arange(0,256), hist_img5); 


# Note that in this case the peak close to 250 are related to the plant and not to a square object, therefore, the threshold must be different from the ones used before. Now, we will set values below 200 to zero and keep the remaining ones.

# In[32]:


#create a matrix with zeros with the same size as the image input
img5_zero_t = np.zeros_like(soybean5_lumi).astype(np.uint32)

#change the values below and above the limits to zero and keep the remaining ones
for x in range(soybean5_lumi.shape[1]):
    for y in range(soybean5_lumi.shape[0]):
        if soybean5_lumi[y,x] < 200: #In this case, we want to keep the brighter pixels
            img5_zero_t[y,x] = 0
        else:
            img5_zero_t[y,x] = soybean5_lumi[y,x]


# In[33]:


#Median filter application
segmented_median5_n10 = medianFilter(img5_zero_t, 10)
segmented_median5_n5 = medianFilter(img5_zero_t, 5)


# In[34]:


#Plotting figures
plt.figure(figsize=(size_img0))
plt.subplot(323); plt.title("Original", size = 15); plt.imshow(soybean5); 
plt.subplot(324); plt.title("Segmented", size = 15); plt.imshow(img5_zero_t); 
plt.subplot(325); plt.title("Segmented + Median filter n =10", size = 15); plt.imshow(segmented_median5_n10); 
plt.subplot(326); plt.title("Segmented + Median filter n =5", size = 15); plt.imshow(segmented_median5_n5);


# Note from the above images how the median filter with kernel size of 10 yielded better results than size of 5. 

# In[36]:


#It was tested the square, diamond and disk morphology operators
img5_erod_diamond = morphology.binary_erosion(segmented_median5_n10, morphology.diamond(5)).astype(np.uint8)
img5_dila_diamond = morphology.binary_dilation(img5_erod_diamond, morphology.diamond(5)).astype(np.uint8)

img5_erod_disk = morphology.binary_erosion(segmented_median5_n10, morphology.disk(5)).astype(np.uint8)
img5_dila_disk = morphology.binary_dilation(img5_erod_disk, morphology.disk(5)).astype(np.uint8)

img5_erod_square = morphology.binary_erosion(segmented_median5_n10, morphology.square(5)).astype(np.uint8)
img5_dila_square = morphology.binary_dilation(img5_erod_square, morphology.square(5)).astype(np.uint8)

plt.figure(figsize=(size_img0))
plt.subplot(321); plt.title("Eroded image - diamond(5)"); plt.imshow(img5_erod_diamond)
plt.subplot(322); plt.title("Dilated image - diamond(5)"); plt.imshow(img5_dila_diamond)

plt.subplot(323); plt.title("Eroded image - disk(5)"); plt.imshow(img5_erod_disk)
plt.subplot(324); plt.title("Dilated image - disk(5)"); plt.imshow(img5_dila_disk)

plt.subplot(325); plt.title("Eroded image - square(5)"); plt.imshow(img5_erod_square)
plt.subplot(326); plt.title("Dilated image - square(5)"); plt.imshow(img5_dila_square)


# Note that in the case where the scheme has a large difference among background and target object, the use of morphological operators helped to improve the image segmentation.
# 
# In this task, we can notice that the use of thresholding based on the RGB to Luminance, can yield results that might be able to segment soybean plants at R8 stage from their background. However, it is not a method that was able to segment the entire background from the soybean plants. It is highlighted that the background and plants have similar color and texture making more difficult the segmentation task.
# 
# As a conclusion, we can assume that it was possible to segment plant and background applying in the color space enhanced with the application of a median filter. And, depending on the image, the use of morphological operators (erosion followed by dilation, can improve the segmentation. However, there is a trade-off on which it may remove important (in this case, part of the plant) pixels from the image.
# 
