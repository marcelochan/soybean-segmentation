#!/usr/bin/env python
# coding: utf-8

# Marcelo Chan Fu Wei
# N.USP: 10630558
# Final Project - Soybean plant segmentation in field conditions at R8 maturity stage
# SCC5830 - Image Processing
# Prof. Moacir Ponti

# Soybean plants at R8 maturity stage are close to the harvest period. Therefore, their leaves fall and the plant stem and pods become brownish. Note that in field conditions, most of the areas present the soil (background) with brown colors and adding the fallen leaves which are also brown, make the segmentation process to become challeging since all the objects within the scheme are brown-related. It will be used images acquired by the author with soybean plants at the R8 maturity stage taken from a proximal RGB sensor (cellphone) with the objective to segment the soybean plant among the background.

# In[92]:


#Libraries required
import numpy as np
import imageio
import matplotlib.pyplot as plt
import skimage.color
import skimage.filters


# In[178]:


#Set of images
soybean0 = imageio.imread('IMG_20190325_113727854.jpg')
soybean1 = imageio.imread('IMG_20190325_113733080.jpg')
soybean2 = imageio.imread('IMG_20190325_114501857.jpg')
soybean3 = imageio.imread('IMG_20190329_114131057.jpg')
soybean4 = imageio.imread('IMG_20190329_114423017.jpg')
soybean5 = imageio.imread('IMG_20190224_151800632.jpg')


# In[201]:


#Function to segment the background and the soybean plant at the R8 maturity stage
def segmentation(image, sigma=2): 

    #skiimage.color.rgb2gray converts RGB image to grayscale 
    gray = skimage.color.rgb2gray(image)
    #Application of a Gaussian filter with a sigma of 2. Note that this values can be changed
    gray = skimage.filters.gaussian(gray, sigma=2)
    #Otsu threshold
    t = skimage.filters.threshold_otsu(gray)
    #Creating a mask with values higher than the Otsu Threshold
    mask = gray > t
    
    #selecting the desired values within the image
    sel = np.zeros_like(image)
    sel[mask] = image[mask]
    
    #Plotting the original image, mask image and segmented image
    plt.figure(figsize = (15,20))
    plt.subplot(1,3,1)
    plt.title("Original Image")
    plt.imshow(image)
    plt.figure(figsize = (15,20))
    plt.subplot(1,3,2)
    plt.title("Mask Image - grayscale")
    plt.imshow(mask, cmap= 'gray')
    plt.figure(figsize = (15,20))
    plt.subplot(1,3,3)
    plt.title("Segmented image")
    plt.imshow(sel)
    print("It was used sigma value of", sigma, "and threshold (t) value of", format(t, '.4f'), "\n")


# It is difficult to design a perfect method to segment the background and soybean plants in field conditions in R8 phenological stage, however, it is still needed to test other methods to improve the segmentation process. Note that the images taken from soybean plants at R8 maturity stage (close to the harvest period) present several obstacles such as background and soybean plant with similar colours, dry and small leaves that are similar to soybeans pods and soybean variety on which some present brighter pods than others and many other variables.

# In[202]:


'''
Note that the Otsu Threshold (t) will not always yield good results as you can see using the image "soja5".
The t value estimated was about 0.53. If we apply a threshold of 0.70, the results would be different on which most of the
background would be removed and only the soybeans would be accounted.

Therefore, more thresholding methods will be tested for segmentation purposes.

'''
segmentation(soybean5, sigma = 2)


# In[ ]:




